package com.hw.db;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class threadControllerTests {
    private Thread t = null;
    private Thread changedT = null;

    private static final String EXAMPLE_SLUG = "slug";
    private static final String EXAMPLE_404_SLUG = "slug404";

    @BeforeEach
    @DisplayName("Initialize Thread Controller")
    void createThreadTest() {
        t = new Thread("author",
                new Timestamp(new Date().getTime()),
                "forum",
                "message",
                EXAMPLE_SLUG,
                "title",
                0);
        changedT = new Thread("author",
                new Timestamp(new Date().getTime()),
                "forum",
                "message",
                EXAMPLE_404_SLUG,
                "titleCHANGED",
                0);
        t.setId(0);
        changedT.setId(0);
    }

    @Test
    @DisplayName("CheckIdOrSlug")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> tMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            tMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(t);
            tMock.when(() -> ThreadDAO.getThreadBySlug(EXAMPLE_SLUG)).thenReturn(t);

            // id-based find
            assertEquals(t, tc.CheckIdOrSlug("0"), "found by id");

            // slug-based find
            assertEquals(t, tc.CheckIdOrSlug(EXAMPLE_SLUG), "found by slug");

            // check null values
            assertNull(tc.CheckIdOrSlug("100"), "not found by id");
            assertNull(tc.CheckIdOrSlug(EXAMPLE_404_SLUG), "not found by slug");
        } catch (Error error) {
            System.out.println(error.getMessage());
        }
    }

    @Test
    @DisplayName("CreatePost")
    void testCreatePost() {
        List<Post> posts = Collections.emptyList();
        try (MockedStatic<ThreadDAO> tMock = Mockito.mockStatic(ThreadDAO.class)) {
            // We also need a user
            try (MockedStatic<UserDAO> uMock = Mockito.mockStatic(UserDAO.class)) {
                threadController tc = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), tc.createPost(EXAMPLE_SLUG, posts), "no existing post");
            } catch (Error error) {
                System.out.println(error.getMessage());
            }
        }
    }

    @Test
    @DisplayName("Posts")
    void testGetPosts() {
        List<Post> posts = Collections.emptyList();
        try (MockedStatic<ThreadDAO> tMock = Mockito.mockStatic(ThreadDAO.class)) {
            tMock.when(() -> ThreadDAO.getThreadBySlug(EXAMPLE_SLUG)).thenReturn(t);

            threadController tc = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), tc.Posts(EXAMPLE_SLUG, 100, 1, "tree", true));
        } catch (Error error) {
            System.out.println(error.getMessage());
        }
    }

    @Test
    @DisplayName("Change")
    void testChange() {
        try (MockedStatic<ThreadDAO> tMock = Mockito.mockStatic(ThreadDAO.class)) {
            tMock.when(() -> ThreadDAO.getThreadBySlug(EXAMPLE_SLUG)).thenReturn(t);
            tMock.when(() -> ThreadDAO.getThreadById(0)).thenReturn(changedT);

            threadController tc = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(changedT), tc.change(EXAMPLE_SLUG, changedT), "editing");
        } catch (Error error) {
            System.out.println(error.getMessage());
        }
    }

    @Test
    @DisplayName("Info")
    void testInfo() {
        try (MockedStatic<ThreadDAO> tMock = Mockito.mockStatic(ThreadDAO.class)) {
            tMock.when(() -> ThreadDAO.getThreadBySlug(EXAMPLE_SLUG)).thenReturn(t);

            threadController tc = new threadController();
            assertEquals(ResponseEntity.status(HttpStatus.OK).body(t), tc.info(EXAMPLE_SLUG));
        }
    }

    @Test
    @DisplayName("CreateVote")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> tMock = Mockito.mockStatic(ThreadDAO.class)) {
            // We also need a user
            try (MockedStatic<UserDAO> uMock = Mockito.mockStatic(UserDAO.class)) {
                User u = new User("elonmusk", "elon@spacex.com", "Elon Musk", "Visionary. We will go to Mars.");
                Vote v = new Vote("elonmusk", 42); // thumbs up if you've read hitchhiker's guide to the galaxy

                tMock.when(() -> ThreadDAO.getThreadBySlug(EXAMPLE_SLUG)).thenReturn(t);
                uMock.when(() -> UserDAO.Info(v.getNickname())).thenReturn(u);

                threadController tc = new threadController();
                assertEquals(ResponseEntity.status(HttpStatus.OK).body(t), tc.createVote(EXAMPLE_SLUG, v));
            }
        }
    }


}
